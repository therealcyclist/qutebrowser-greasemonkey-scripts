// ==UserScript==
// @name           Twitter to Nitter
// @namespace      therealcyclist
// @description    Redirects Twitter URLs to Nitter URLs
// @match          https://twitter.com/*
// @match          http://twitter.com/*
// @match          https://www.twitter.com/*
// @match          http://www.twitter.com/*
// @run-at         document-start
// ==/UserScript==

const nitterInstances = [
  'nitter.net',
  'nitter.sethforprivacy.com',
  'nitter.adminforge.de',
];

function isValidURL(url) {
  try {
    new URL(url);
    return true;
  } catch (e) {
    return false;
  }
}

function isAllowedDomain(url, allowedDomains) {
  const parsedURL = new URL(url);
  return allowedDomains.some(domain => parsedURL.hostname === domain);
}

function getNextNitterInstance() {
  const currentIndex = parseInt(localStorage.getItem('nitterIndex') || '0', 10);
  const nextIndex = (currentIndex + 1) % nitterInstances.length;
  localStorage.setItem('nitterIndex', nextIndex.toString());
  return nitterInstances[nextIndex];
}

if (window.location.hostname.includes('twitter.com')) {
  const nitterInstance = getNextNitterInstance();
  const newURL = window.location.href.replace('twitter.com', nitterInstance);

  if (isValidURL(newURL) && isAllowedDomain(newURL, nitterInstances)) {
    window.location.replace(newURL);
  } else {
    console.error('Invalid URL or domain not allowed:', newURL);
  }
}
